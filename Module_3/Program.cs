﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        protected Program() { }
        static void Main(string[] args)
        {
            
            Task1 t1 = new Task1();
            Console.WriteLine(t1.Multiplication(t1.ParseAndValidateIntegerNumber("5"), t1.ParseAndValidateIntegerNumber("4")));
            Console.WriteLine(t1.Multiplication(t1.ParseAndValidateIntegerNumber("-5"), t1.ParseAndValidateIntegerNumber("4")));
            Console.WriteLine(t1.Multiplication(t1.ParseAndValidateIntegerNumber("-5"), t1.ParseAndValidateIntegerNumber("-4")));
            Console.WriteLine(t1.Multiplication(t1.ParseAndValidateIntegerNumber("-5"), t1.ParseAndValidateIntegerNumber("-4.0")));
            Console.WriteLine(t1.Multiplication(t1.ParseAndValidateIntegerNumber("-5"), t1.ParseAndValidateIntegerNumber("-4a")));
            
            Task2 t2 = new Task2();
            int r;
            if (t2.TryParseNaturalNumber("1", out r))
            {
                foreach (int a in t2.GetEvenNumbers(r))
                {
                    Console.WriteLine(a);
                }
            }
            

            int s, d;
            Task3 t3 = new Task3();
            if (t3.TryParseNaturalNumber("5683", out s) && t3.TryParseNaturalNumber("8", out d))
            {
                Console.WriteLine(t3.RemoveDigitFromNumber(s, d));
            }
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int result;
            if (int.TryParse(source, out result)) {
                return result;
            }
            else
            {
                throw new ArgumentException("source is not valid integer number");
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int m = 0;

            bool signNum2 = num2 < 0;

            for (int i = 1; i <= Math.Abs(num2); i++)
            {
                m = m + num1;
            }

            if(signNum2) 
            { 
                m = -m; 
            }

            return m;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = 0;
            bool flag = false;
            uint uresult;
            if (uint.TryParse(input, out uresult))
            {
                result =  (int)uresult;
                flag = true;
            }

            return flag;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> l = new List<int>();
            for (int i = 0; i <= naturalNumber; i++)
            {
                if (i % 2 == 0)
                {
                    l.Add(i);
                }
            }
            return l;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = 0;
            bool flag = false;
            uint uresult;
            if (uint.TryParse(input, out uresult))
            {
                result = (int)uresult;
                flag = true;
            }

            return flag;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            return source.ToString().Replace(digitToRemove.ToString(), "");
        }
    }
}
